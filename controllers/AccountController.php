<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\controllers\My_Controller;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAOAccountRole;
use Exception;

class AccountController extends My_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function createAccount(){
        $dao = new DAOAccount($this->inputPost());
        $lastId = $dao->create();

        $role = new DAOAccountRole(array(
            "account_id" => $lastId,
        ));

        $role->create();
    }

    public function retrieveAccount(){
        $dao = new DAOAccount;
        $account = $dao->retrieve($this->inputGet()["id"]);
        echo json_encode($account->returnAll());
    }

    public function getAllAccounts(){
        $dao = new DAOAccount;
        $entities = $dao->getAll();

        $array = [];
        foreach ($entities as $entity) {
            array_push($array, $entity->returnAll());
        }
        
        echo json_encode($array);
    }

    public function updateAccount(){
        $dao = new DAOAccount($this->inputPost());
        $dao->update([]);
    }

    public function removeAccount(){
        $dao = new DAOAccount;
        $dao->delete($this->inputPut()['id']);
    }

}