<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\Groups;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOGroups extends MY_DAO {

	public function __construct($array = []){
		parent::__construct();
		$this->entity = new Groups($array);
	}

/* ____________________Crud methods____________________*/


	public function create ($array = []){

		$sql = "INSERT INTO groups (nom) VALUES('" . $this->entity->getNom() . "')";
		var_dump($sql);
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM groups WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$this->entity = new Groups($result);
		return $this->entity;
	}


	public function update ($array = []){

		$sql = "UPDATE groups SET nom = '" . $this->entity->getNom() ."' WHERE id = ". $this->entity->getId();
		var_dump($sql);
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM groups WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM groups";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			array_push($entities,new Groups($result));
		}
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM groups";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = " . $value . "'";
			$i++;
		}
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			array_push($entities,new Groups($result));
		}
		return $entities;
	}
}