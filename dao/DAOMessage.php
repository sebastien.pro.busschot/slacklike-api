<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\Message;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOMessage extends MY_DAO {

	public function __construct($array = []){
		parent::__construct();
		$this->entity = new Message($array);
	}

/* ____________________Crud methods____________________*/


	public function create ($array = []){

		$sql = "INSERT INTO message (content,file_id) VALUES('" . $this->entity->getContent() . "','" . $this->entity->getFile_id() . "')";
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM message WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$this->entity = new Message($result);
		
		return $this->entity;
	}


	public function update ($array = []){

		$sql = "UPDATE message SET content = '" . $this->entity->getContent() ."',file_id = '" . $this->entity->getFile_id() ."' WHERE id = ". $this->entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM message WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM message";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();

		$entities = array();
		foreach($results as $result){
			$this->entity = new Message($result);
			array_push($entities,$this->entity);
		}
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM message";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = " . $value . "'";
			$i++;
		}
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			$this->entity = new Message($result);
			array_push($entities,$this->entity);
		}
		return $entities;
	}
}