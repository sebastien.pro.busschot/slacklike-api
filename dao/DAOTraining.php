<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\Training;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOTraining extends MY_DAO {

	public function __construct($array = array()){
		parent::__construct();
		$this->entity = new Training($array);
	}

/* ____________________Crud methods____________________*/


	public function create ($array=[]){

		$sql = "INSERT INTO training (nom) VALUES('" . $this->entity->getNom() . "')";
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM training WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$this->entity = new Training($result);

		return $this->entity;
	}


	public function update ($array=[]){

		$sql = "UPDATE training SET nom = '" . $this->entity->getNom() ."' WHERE id = ". $this->entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM training WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM training";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();

		$entities = array();
		foreach($results as $result){
			array_push($entities,new Training($result));
		}
		return $entities;
	}


	public function getAllBy ($filter){
		// $sql = "SELECT * FROM training";
		// $i = 0;
		// foreach($filter as $key => $value){
		// 	if($i===0){
		// 		$sql .= " WHERE ";
		// 	} else {
		// 		$sql .= " AND ";
		// 	}
		// 	$sql .= $key . " = " . $value . "'";
		// 	$i++;
		// }
		// $entities = array();
		// $statement = $this->getPdo()->query($sql);
		// $results = $statement->fetchAll();
		// foreach($results as $result){
		// 	$entity = new Training;
		// 	$entity->setId($result['id']);
		// 	$entity->setNom($result['nom']);
		// 	array_push($entities,$entity);
		// }
		// return $entities;
	}
}