<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\Account;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOAccount extends MY_DAO {

	public function __construct($array = array()){
		parent::__construct();
		$this->entity = new Account($array);
	}

/* ____________________Crud methods____________________*/

	public function create ($array = []){

		$sql = "INSERT INTO account (firstname,lastname,email) VALUES('" . $this->entity->getFirstname() . "','" . $this->entity->getLastname() . "','" . $this->entity->getEmail() . "')";
		$this->getPdo()->query($sql);

		return $this->getPdo()->lastInsertId(); 
	}


	public function retrieve($id){

		$sql = "SELECT * FROM account WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$this->entity = new Account($result);
		
		return $this->entity;
	}


	public function update($array){

		$sql = "UPDATE account SET firstname = '" . $this->entity->getFirstname() ."',lastname = '" . $this->entity->getLastname() ."',email = '" . $this->entity->getEmail() ."' WHERE id = ". $this->entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM account WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM account";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();

		$entities = [];
		foreach ($results as $row) {
			array_push($entities, new Account($row));
		}
		
		return $entities;
	}


	public function getAllBy ($filter){
		// $sql = "SELECT * FROM account";
		// $i = 0;
		// foreach($filter as $key => $value){
		// 	if($i===0){
		// 		$sql .= " WHERE ";
		// 	} else {
		// 		$sql .= " AND ";
		// 	}
		// 	$sql .= $key . " = " . $value . "'";
		// 	$i++;
		// }
		// $entities = array();
		// $statement = $this->getPdo()->query($sql);
		// $results = $statement->fetchAll();
		// foreach($results as $result){
		// 	$this->entity = new Account($result);
		// 	array_push($entities,$this->entity);
		// }
		// return $entities;
	}
}