<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\AccountRole;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOAccountRole extends MY_DAO {

	public function __construct($array = array()){
		parent::__construct();
		$this->entity = new AccountRole($array);
	}

/* ____________________Crud methods____________________*/


	public function create ($array = []){

		$sql = "INSERT INTO account_role (role_id,account_id,training_id) VALUES('" . $this->entity->getRole_id() . "','" . $this->entity->getAccount_id() . "','" . $this->entity->getTraining_id() . "')";
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM account_role WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$this->entity = new AccountRole($result);

		return $this->entity;
	}


	public function update ($array = []){

		$sql = "UPDATE account_role SET role_id = '" . $this->entity->getRole_id() ."',account_id = '" . $this->entity->getAccount_id() ."',training_id = '" . $this->entity->getTraining_id() ."' WHERE id = ". $this->entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}


	public function delete ($array){
		
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM account_role";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			array_push($entities,new AccountRole($result));
		}
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM account_role";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = '" . $value . "'";
			$i++;
		}
		$statement = $this->getPdo()->query($sql);
		echo $sql;
		$results = $statement->fetchAll();

		$entities = array();
		foreach($results as $result){
			array_push($entities,new AccountRole($result));
		}
		return $entities;
	}
}