<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\Address;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOAddress extends MY_DAO {

	public function __construct($array = []){
		parent::__construct();
		$this->entity = new Address($array);
	}

/* ____________________Crud methods____________________*/


	public function create ($array = []){

		$sql = "INSERT INTO address (account_id,address,city,zipcode) VALUES('" . $this->entity->getAccount_id() . "',' ". $this->entity->getAddress() . "','" . $this->entity->getCity() ." ','" . $this->entity->getZipcode() . "')";
		var_dump($sql);
		$this->getPdo()->query($sql);
		
	}

		
	public function retrieve($id)
	{

		$sql = "SELECT * FROM address WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		$this->entity = new Address($result);
		
		return $this->entity;
	}

	public function update ($array = []){

		$sql = "UPDATE address SET account_id = '" . $this->entity->getAccount_id() ."',address = '" . $this->entity->getAddress() ."',city = '" . $this->entity->getCity() ."',zipcode = '" . $this->entity->getZipcode() ."' WHERE id = ". $this->entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}

	public function delete($id)
	{

		$sql = "DELETE FROM address WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

	/* ____________________Repository methods____________________*/


	public function getAll()
	{
		$sql = "SELECT * FROM address";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();

		foreach($results as $result){
			array_push($entities,new Address($result));
		}

		return $entities;
	}


	public function getAllBy($filter)
	{
		$sql = "SELECT * FROM address";
		$i = 0;
		foreach ($filter as $key => $value) {
			if ($i === 0) {
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = " . $value . "'";
			$i++;
		
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			array_push($entities,new Address($result));
		}
		return $entities;
	}
		}
}
