<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\MessageAccount;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOMessageAccount extends MY_DAO {

	public function __construct($array = []){
		parent::__construct();
		$this->entity = new MessageAccount($array);
	}

/* ____________________Crud methods____________________*/


	public function create ($array = []){

		$sql = "INSERT INTO message_account (message_id,sender_id,receiver_id,state) VALUES('" . $this->entity->getMessage_id() . "','" . $this->entity->getSender_id() . "','" . $this->entity->getReceiver_id() . "','" . $this->entity->getState() . "')";
		var_dump($sql);
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM message_account WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$entity = new MessageAccount($result);
		return $entity;
	}


	public function update ($array = []){

		$sql = "UPDATE message_account SET message_id = '" . $this->entity->getMessage_id() ."',sender_id = '" . $this->entity->getSender_id() ."',receiver_id = '" . $this->entity->getReceiver_id() ."',state = '" . $this->entity->getState() ."' WHERE id = ". $this->entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM message_account WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM message_account";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			array_push($entities,new MessageAccount($result));
		}
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM message_account";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = '" . $value . "'";
			$i++;
		}
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			array_push($entities,new MessageAccount($result));
		}
		return $entities;
	}
}