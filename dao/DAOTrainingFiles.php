<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\TrainingFiles;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOTrainingFiles extends MY_DAO {

	public function __construct($array = array()){
		parent::__construct();
		$this->entity = new TrainingFiles($array);
	}

/* ____________________Crud methods____________________*/



	public function create ($array = []){


		$sql = "INSERT INTO training_files (id_training,id_files) VALUES('" . $this->entity->getId_training() . "','" . $this->entity->getId_files() . "')";
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM training_files WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$this->entity = new TrainingFiles($result);

		return $this->entity;
		
	}


	public function update ($array){

		$sql = "UPDATE training_files SET id_training = '" . $this->entity->getId_training() ."',id_files = '" . $this->entity->getId_files() ." WHERE id = ". $this->entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated";
		} else {
			echo "Failed";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM training_files WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM account";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();

		$entities = [];
		foreach ($results as $row) {
			array_push($entities, new Account($row));
		}
		
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM training_files";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = '" . $value . "'";
			$i++;
		}
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			$this->entity = new TrainingFiles($result);
			array_push($entities,$this->entity);
	
		}
		return $entities;
	}
}