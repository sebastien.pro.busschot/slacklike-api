<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\dao\MY_DAO;
use BWB\Framework\mvc\models\GroupAccount;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOGroupAccount extends MY_DAO {

	public function __construct($array = []){
		parent::__construct();
		$this->entity = new GroupAccount($array);
	}

/* ____________________Crud methods____________________*/


	public function create ($array = []){

		$sql = "INSERT INTO group_account (account_id,groups_id) VALUES('" . $this->entity->getAccount_id() . "','" . $this->entity->getGroups_id() . "')";
		var_dump($sql);
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM group_account WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$this->entity = new GroupAccount($result);
		return $this->entity;
	}


	public function update ($array = []){

		//! pas besoin !! Table avec uniquement clés étrangères
	}


	public function delete ($id = null){

		$sql = "DELETE FROM group_account WHERE account_id= '". $this->entity->getAccount_id()."' AND groups_id = '".$this->entity->getGroups_id()."'";  
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM group_account";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			array_push($entities,new GroupAccount($result));
		}
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM group_account";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = " . $value . "'";
			$i++;
		}
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			array_push($entities,new GroupAccount($result));
		}
		return $entities;
	}
}