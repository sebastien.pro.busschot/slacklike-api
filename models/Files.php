<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class Files extends MY_Model{

		private $id;

		private $name;

		private $path;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getName (){
		return $this->name;
	}


	public function setName ($val){
		$this->name = $val;
	}


	public function getPath (){
		return $this->path;
	}


	public function setPath ($val){
		$this->path = $val;
	}

}