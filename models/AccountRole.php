<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class AccountRole extends MY_Model{

		private $role_id;

		private $account_id;

		private $training_id;


/* ____________________ Getter and Setter Part ____________________ */


	public function getRole_id (){
		return $this->role_id;
	}


	public function setRole_id ($val){
		$this->role_id = $val;
	}


	public function getAccount_id (){
		return $this->account_id;
	}


	public function setAccount_id ($val){
		$this->account_id = $val;
	}


	public function getTraining_id (){
		return $this->training_id;
	}


	public function setTraining_id ($val){
		$this->training_id = $val;
	}

}