<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class Address extends MY_Model{

		private $id;

		private $account_id;

		private $address;

		private $city;

		private $zipcode;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getAccount_id (){
		return $this->account_id;
	}


	public function setAccount_id ($val){
		$this->account_id = $val;
	}


	public function getAddress (){
		return $this->address;
	}


	public function setAddress ($val){
		$this->address = $val;
	}


	public function getCity (){
		return $this->city;
	}


	public function setCity ($val){
		$this->city = $val;
	}


	public function getZipcode (){
		return $this->zipcode;
	}


	public function setZipcode ($val){
		$this->zipcode = $val;
	}

}