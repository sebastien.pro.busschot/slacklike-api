<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class KeyAccount extends MY_Model{

		private $id;

		private $value;

		private $account_id;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getValue (){
		return $this->value;
	}


	public function setValue ($val){
		$this->value = $val;
	}


	public function getAccount_id (){
		return $this->account_id;
	}


	public function setAccount_id ($val){
		$this->account_id = $val;
	}

}