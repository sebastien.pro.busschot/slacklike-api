<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class Account extends MY_Model{

		private $id;

		private $firstname;

		private $lastname;

		private $email;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getFirstname (){
		return $this->firstname;
	}


	public function setFirstname ($val){
		$this->firstname = $val;
	}


	public function getLastname (){
		return $this->lastname;
	}


	public function setLastname ($val){
		$this->lastname = $val;
	}


	public function getEmail (){
		return $this->email;
	}


	public function setEmail ($val){
		$this->email = $val;
	}

}