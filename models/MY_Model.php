<?php
namespace BWB\Framework\mvc\models;


abstract Class MY_Model {
    private $keys = [];

    public function __construct($array){
        foreach ($array as $key => $value)
        {
            // On récupère le nom du setter correspondant à l'attribut.
            $method = 'set'.ucfirst($key);
                
            // Si le setter correspondant existe.
            if (method_exists($this, $method))
            {
                // On appelle le setter.
                $this->$method(strip_tags($value));
                array_push($this->keys, $key);
            }
        }
    }

    public function returnAll(){
        $array = [];
        foreach ($this->keys as $key) {
            $get = 'get'.ucfirst($key);
            $array[$key] = $this->$get();
        }
        return $array;
    }

}