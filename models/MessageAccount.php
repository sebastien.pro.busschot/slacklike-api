<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class MessageAccount extends MY_Model{

		private $id;

		private $message_id;

		private $sender_id;

		private $receiver_id;

		private $state;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getMessage_id (){
		return $this->message_id;
	}


	public function setMessage_id ($val){
		$this->message_id = $val;
	}


	public function getSender_id (){
		return $this->sender_id;
	}


	public function setSender_id ($val){
		$this->sender_id = $val;
	}


	public function getReceiver_id (){
		return $this->receiver_id;
	}


	public function setReceiver_id ($val){
		$this->receiver_id = $val;
	}


	public function getState (){
		return $this->state;
	}


	public function setState ($val){
		$this->state = $val;
	}

}