<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class Message extends MY_Model{

		private $id;

		private $content;

		private $file_id;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getContent (){
		return $this->content;
	}


	public function setContent ($val){
		$this->content = $val;
	}


	public function getFile_id (){
		return $this->file_id;
	}


	public function setFile_id ($val){
		$this->file_id = $val;
	}

}