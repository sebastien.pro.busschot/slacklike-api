<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class TrainingFiles extends MY_Model{

		private $id_training;

		private $id_files;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId_training (){
		return $this->id_training;
	}


	public function setId_training ($val){
		$this->id_training = $val;
	}


	public function getId_files (){
		return $this->id_files;
	}


	public function setId_files ($val){
		$this->id_files = $val;
	}

}