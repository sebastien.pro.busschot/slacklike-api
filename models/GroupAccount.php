<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\MY_Model;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class GroupAccount extends MY_Model{

		private $account_id;

		private $groups_id;


/* ____________________ Getter and Setter Part ____________________ */


	public function getAccount_id (){
		return $this->account_id;
	}


	public function setAccount_id ($val){
		$this->account_id = $val;
	}


	public function getGroups_id (){
		return $this->groups_id;
	}


	public function setGroups_id ($val){
		$this->groups_id = $val;
	}

}